
public class Cell 
{
	private boolean open;
	private boolean full;
	
	public Cell(boolean isOpen, boolean isFull) 
	{
		open = isOpen;
		full = isFull;
	}
	
	public Cell()
	{
		open = false;
		full = false;
	}
	
	public boolean isOpen()
	{
		return open;
	}
	
	public boolean isFull()
	{
		return full;
	}

	public void setOpen(boolean b)
	{
		open = b;
	}
	
	public void setFull(boolean b)
	{
		full = b;
	}
	
	public String toString()
	{
		return ("Full: " + full + ", Open: " + open);
	}
}
