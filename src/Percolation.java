public class Percolation
{
	private int n;
	private WeightedQuickUnionUF uf;
	private Cell[] cells;
	
	public Percolation(int a) 
	{
		n = a;
		uf = new WeightedQuickUnionUF(n*n + 2*n + 2);
		cells = new Cell[n*n + n];
		
		for(int i = 1; i <= n; i++)
		{
			for(int j = 1; j <= n; j++)
			{
				cells[n*i + j - 1] = new Cell(false,false);
			}
		}
	}

	public void open(int i, int j)
	{
		cells[n*i + j - 1].setOpen(true);		
	}
	
	public boolean isOpen(int i, int j)
	{
		return cells[n*i + j - 1].isOpen();
	}
	
	public boolean isFull(int i, int j)
	{
		
		if((n*i + j - 1) > n*n && isOpen(i,j))
		{
			uf.union(n*n+n+1, n*i+j);
//			System.out.println("Top check: N = " + n + ", i = " + i + ", j = " + j );
		}
		
		if((n*i + j - 1 < 2*n) && isOpen(i,j))
		{
			cells[n*i + j - 1].setFull(true);
			uf.union(0, n*i + j);
//			System.out.println("Bottom check: N = " + n + ", i = " + i + ", j = " + j );
		}
		
		if ((n*i + j - 1) >= n && (n*i + j - 1) < n*n + n && isOpen(i,j))
		{
			if(uf.connected(0,n*i+j + n))
			{
				cells[n*i + j - 1].setFull(true);
				uf.union(0, n*i + j);
//				System.out.println("Up check: N = " + n + ", i = " + i + ", j = " + j );
			}
			if(uf.connected(0, n*i + j - n))
			{
				cells[n*i + j - 1].setFull(true);
				uf.union(0, n*i + j);
//				System.out.println("Down check : N = " + n + " , i = " + i + ", j = " + j);
			}
			
			if((j != n && uf.connected(0, (n*i + j + 1))))
			{
				cells[n*i + j - 1].setFull(true);
				uf.union(0, n*i + j);
//				System.out.println("Right check: N = " + n + ", i = " + i + ", j = " + j );
			}
			if(j != 1 && uf.connected(0, n*i + j - 1))
			{
				cells[n*i + j - 1].setFull(true);
				uf.union(0, n*i + j);
//				System.out.println("Left check: N = " + n + ", i = " + i + ", j = " + j);
			}
		}
		cells[n*i + j - 1].setFull(false);
		return uf.connected(0,n*i + j);
	}
	
	public boolean percolates()
	{
		return uf.connected(0, n*n + n + 1);
	}
}