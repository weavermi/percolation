import java.util.Scanner;


public class PercolationStats 
{
	private Percolation[] p;
	private int n;
	private int t;
	private int[] open;
	private int nSites;
	
	public PercolationStats(int N, int T)
	{
		System.out.println("Beginning percolation");
		this.n = N;
		this.t = T;
		
		open = new int[T];
		
		nSites = N * N;
		
		p = new Percolation[T];
		int a;
		int b;
		for(int i = 0; i < T; i++)
		{
			open[i] = 0;
			p[i] = new Percolation(n);
			
			while(!p[i].percolates())
			{
				a = StdRandom.uniform(n) + 1;
				b = StdRandom.uniform(n) + 1;
//				if(!p[i].isOpen(a, b))
//				{
//					System.out.println("p["+i+"].open("+a+","+b+")");
					p[i].open(a, b);
					open[i] = open[i] + 1;
					p[i].isFull(a, b);				
//				}
//				else
//				{
//					System.out.println("Full");
//				}
			}
		}	
	}
	
	public double mean()
	{
//		System.out.println("Discovering mean");
		double sum = 0;
		
		for(int i = 0; i < t; i++)
		{
			sum += open[i] / (n * n);
		}
		return sum / t / n;
//		return StdStats.mean(open);
	}
	
	public double stddev()
	{
		double sum = 0;
		double mean = mean() * n;
		
		for(int i = 0; i < open.length; i++)
		{
			sum = (open[i] - mean)*(open[i] - mean);
		}
		
		return Math.sqrt(sum / (t - 1)*n);
	}
	
	public double confidenceLo()
	{
		return mean() - ((1.96*stddev())/Math.sqrt(t));
	}
	
	public double confidenceHi()
	{
		return mean()+ ((1.96*stddev())/Math.sqrt(t));
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
	while(true)
	{
		System.out.print("Input: ");
		String s = input.nextLine();
		int index = s.indexOf(" ");
		
		String size = s.substring(0,index);
		String times = s.substring(index + 1, s.length());
		
		int A = Integer.parseInt(size);
		int B = Integer.parseInt(times);
		
		System.out.println(A + " " + B);
		
		PercolationStats stat = new PercolationStats(A,B);
		System.out.println("mean = " + stat.mean());
		System.out.println("stddev = " + stat.stddev());
		System.out.println("low = " + stat.confidenceLo());
		System.out.println("high = " + stat.confidenceHi()+"\n");
	}
	}
}
